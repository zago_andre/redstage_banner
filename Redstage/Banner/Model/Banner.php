<?php

namespace Redstage\Banner\Model;

class Banner extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
	const CACHE_TAG = 'redstage_banner_banner';
	protected $_cacheTag = 'redstage_banner_banner';
	protected $_eventPrefix = 'redstage_banner_banner';

	protected function _construct()
	{
		$this->_init('Redstage\Banner\Model\ResourceModel\Banner');
	}

	public function getIdentities()
	{
		return [self::CACHE_TAG . '_' . $this->getId()];
	}

	public function getDefaultValues()
	{
		$values = [];

		return $values;
	}
}