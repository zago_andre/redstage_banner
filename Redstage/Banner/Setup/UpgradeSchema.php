<?php

namespace Redstage\Banner\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements \Magento\Framework\Setup\UpgradeSchemaInterface
{

	public function upgrade(\Magento\Framework\Setup\SchemaSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
	{
		$installer = $setup;
		$installer->startSetup();

		if (version_compare($context->getVersion(), '1.0.1') < 0 && !$installer->tableExists('redstage_banner_banner')) {
			$table = $installer->getConnection()->newTable(
				$installer->getTable('redstage_banner_banner')
			)
			->addColumn(
				'banner_id',
				\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
				null,
				[
					'identity' => true,
					'nullable' => false,
					'primary'  => true,
					'unsigned' => true,
				],
				'Banner ID'
			)			
			->addColumn(
				'url',
				\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
				255,
				[],
				'Banner Link'
			)
			->addColumn(
				'content',
				\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
				'64k',
				[],
				'Banner Content'
			)			
			->addColumn(
				'enabled',
				\Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
				1,
				[],
				'Banner Is Enabled?'
			)			
			->addColumn(
				'created_at',
				\Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
				null,
				['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
				'Created At'
			)->addColumn(
				'updated_at',
				\Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
				null,
				['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
				'Updated At'
			)
			->setComment('Banner Table');

			$installer->getConnection()->createTable($table);
						
		}
		$installer->endSetup();
	}
}