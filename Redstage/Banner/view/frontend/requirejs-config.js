var config = {    
    paths: {            
        "owlcarousel": "Redstage_Banner/js/owl.carousel.min"                
    },   
    shim: {
        'owlcarousel': {
            deps: ['jquery']
        }
    }
};